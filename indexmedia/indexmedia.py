#!/opt/local/bin/python

# Created By: Erik Hentell
# Copyright: Public Domain
# 
# This script creates an HTML file with a group of
# links to a CGI script that delivers media.
#
# Arguments: 1. File path to a target HTML file
#            2. File path to folders to search for files to use
#            3. URL path to a CGI script that will deliver the media

import sys
import os
import pathlib
from urllib.parse import quote

varFile = None
varContentText = ""

# Returns the first argument (see above)
# arguments: none
# returns: string
# side effects: none
def retrieveHTMLFilePath() :
	return sys.argv[1]

# Returns the third argument (see above)
# arguments: none
# returns: string
# side effects: none
def retrieveCGIURL() :
	return sys.argv[3]

# Returns the second argument (see above)
# arguments: none
# returns: string
# side effects: none
def retrieveScanDirectory() :
	return sys.argv[2]

# Checks if the HTML file already exists
# arguments: string
# returns: integer (0 = TRUE, 1 = FALSE)
# external calls: calls Python os library to check if a file exists
def doesFileExist() :
	if(os.path.isfile(retrieveHTMLFilePath())) :
		return 0
	else :
		return 1

# Deletes a target file
# arguments: none
# returns: none
# external calls: calls Python os library to delete a file 
def deleteFile() :
	os.remove(retrieveHTMLFilePath())

# Creates an HTML file and stores it in a global variable
# arguments: none
# returns: none
# side effects: uses a global variable, varFile, to hold the File object
# external calls: uses open() to create the necessary File object
def createPlaylistFile() :
	global varFile
	varFile = open(retrieveHTMLFilePath(), "w")

# Appends an HTML element containing a link to a global text variable
# arguments: string, string
# returns: none
# side effects: uses a global variable, varContentText, to hold the 
#               constructed set of HTML links. Also, reads from 
#		template files to pull the necessary HTML to 
#		construct the final HTML file
# external calls: uses open() and close()
def appendLine(textToAdd, namestr) :
	global varContentText

	varTemplateFile = open("./linkcontainer.template", "rt")
	varTemplateText = varTemplateFile.read()
	varTemplateFile.close()

	varReplace0 = retrieveCGIURL() + "?" + textToAdd
	varReplace1 = varTemplateText.replace("@@REPLACE1@@", varReplace0)
	varReplace2 = varReplace1.replace("@@REPLACE2@@", namestr) 

	varContentText = varContentText + varReplace2

# Percent-encodes a given string
# arguments: string
# returns: none
# external calls: calls urllib.parse.quote() to make the 
#                 encoded string
def encodeText(textToEncode) :
	return quote(textToEncode)

# Checks the file name extension and returns TRUE if it matches a pre-defined
# set. Otherwise, it returns FALSE
# arguments: none
# returns: boolean
def checkEnding(candidate):
    if (file.endswith("mpg")): 
        return True
    elif (file.endswith("mpeg")): 
        return True
    elif (file.endswith("wmv")): 
        return True
    elif (file.endswith("mp4")): 
        return True
    elif (file.endswith("avi")): 
        return True
    elif (file.endswith("flv")): 
        return True
    elif (file.endswith("mov")): 
        return True
    elif (file.endswith("apng")): 
        return True
    elif (file.endswith("avif")): 
        return True
    elif (file.endswith("gif")): 
        return True
    elif (file.endswith("jpg")): 
        return True
    elif (file.endswith("jpeg")): 
        return True
    elif (file.endswith("png")): 
        return True
    elif (file.endswith("svg")): 
        return True
    elif (file.endswith("webp")): 
        return True
    elif (file.endswith("bmp")): 
        return True
    elif (file.endswith("tif")): 
        return True
    elif (file.endswith("tiff")): 
        return True
    elif (file.endswith("mp3")): 
        return True
    elif (file.endswith("m4a")): 
        return True
    elif (file.endswith("aac")): 
        return True
    elif (file.endswith("flac")): 
        return True
    elif (file.endswith("ogg")): 
        return True
    else:
        return False

# Scans a given directory and processes files with the appropriate extensions
# arguments: none
# returns: none
# side effects: calls appendLine() and encodeText() to deliver data
#               these functions, in turn, modify a global variable
# external calls: os.walk(), file.endswith()
def scanDirectories() :
	for root, dirs, files in os.walk(retrieveScanDirectory()) :
		for file in files :
			if(checkEndings()):
				appendLine(encodeText(os.path.join(root, file)), file)

# Writes the text from a global variable to a file object held
# by a global variable
# arguments: none
# returns: none
# side effects: writes to a file held by a global variable, and 
#               pulls from a global variable. Also pulls HTML
# 		data from template files to get the HTML data
#		for constructing the final HTML file
# external calls: open(), close(), write()
def writeContent() :
	global varFile
	global varContentText

	varTemplateFile = open("./html.template", "rt")
	varTemplateText = varTemplateFile.read()
	varTemplateFile.close()

	varFinalText = varTemplateText.replace("@@REPLACE@@", varContentText)

	varFile.write(varFinalText)

	varFile.close()

# -------------

# Check if the target HTML file already exists,
# delete if it does, and create a new version. Otherwise,
# just create a new version
if (doesFileExist() == 0) :
	deleteFile()
	createPlaylistFile()
else : 
	createPlaylistFile()

# Walk through the directories and record the desired files
scanDirectories()

# Write the data into an HTML file
writeContent()
