#!/opt/local/bin/python

# Created By: Erik Hentell
# Copyright: Public Domain
# 
# This CGI script pulls a file path that is
# delivered to it and returns binary data from
# the indicated file
#
# Arguments: None. This script pulls from standard CGI
#            environment variables

import os
import cgi
import cgitb
import urllib.parse
import sys

# For error tracking
cgitb.enable()

varFilePath = urllib.parse.unquote(os.environ['QUERY_STRING'])

# Checks if a file actually exists where it has been indicated
# arguments: none
# returns: integer (0 = TRUE, 1 = FALSE)
# external calls: os.path.isfile()
def doesFileExist() :
	global varFilePath 

	if(os.path.isfile(varFilePath)) :
		return 0
	else : 
		return 1

# Breaks down a file path to extract the filename extension
# arguments: none
# returns: string
def retrieveFileExtension() : 
	global varFilePath
	varExtension = ""
	varCollectionLength = 0
	
	varSet = varFilePath.split(".")
	varCollectionLength = len(varSet)

	varExtension = varSet[varCollectionLength - 1]

	return varExtension

# Given a string marking the extension, return a MIME type
# arguments: string
# returns: string
def retrieveMimeType(extensionStr) : 
	if(extensionStr == "avi") : 
		return "video/x-msvideo"
	elif(extensionStr == "mp4") :
		return "video/mp4"
	elif(extensionStr == "flv") :
		return "video/x-flv"
	elif(extensionStr == "mov") :
		return "video/quicktime"
	elif(extensionStr == "wmv") :
		return "video/x-ms-wmv"
	elif(extensionStr == "mpeg" or extensionStr == "mpg") :
		return "video/mpeg"

# Construct and deliver a "Content-Type" string for HTTP delivery
# arguments: none
# returns: string
def retrieveContentType() : 
    varTempStr = "Content-Type: " + retrieveMimeType(retrieveFileExtension()) + "\r\n\r\n"

    return varTempStr

# Open a file, read-only and binary, and output it to stdout
# arguments: string
# returns: none
# side effects: writing to the stdout buffer
# external calls: sys.stdout.buffer.write(), file.read()
def retrieveFile(filePath) :
    varFile = open(filePath, 'rb')

    sys.stdout.buffer.write(bytes(retrieveContentType(), encoding='utf8'))
    sys.stdout.buffer.write(varFile.read())

    varFile.close()

# -----------------

# Check to see if the file exists. If not, issue
# a failure message. If so, start the process of
# retrieving and delivering the file
if(doesFileExist() == 0) : 
	retrieveFile(varFilePath)
else : 
	print ("Content-Type: text/html")
	print ()
	print ("<html><head><title>Error</title></head<body>There was an error retrieving the file</body></html>")
