#!/opt/local/bin/python

# Created By: Erik Hentell
# Copyright: Public Domain
# 
# This CGI script indexes a given folder hierarchy and returns
# a text file containing an HTML snippet that will be incorporated
# into a pre-existing JavaScript-powered web page
#
# Arguments: 1. File path to folders to search for files to use
#            2. URL path to a CGI script that will deliver the media

import sys
import os
import cgi
import cgitb
import pathlib
from urllib.parse import parse_qs
from urllib.parse import quote

# For error tracking
cgitb.enable()

# contains text that is meant to be 
# delivered through CGI output
varContentText = ""

# Returns a specified query string argument
# arguments: integer
# returns: string
# external calls: calls urlib.parse and os.environ
def retrieveQuery(optstring):
    # varQueries is expected to be composed of:
    # 'version', 'scanpath', and 'cgipath'
    varQueries = parse_qs(os.environ['QUERY_STRING'])

    if (varQueries['versionnum'][0] == "1") :
        if (optstring == 'scanpath') :
            return varQueries['scanpath'][0]
        elif(optstring == 'cgipath') :
            return varQueries['cgipath'][0]
    else :
#        print("Content-Type: text/plain; charset=UTF-8")
#        print()
#        print("Version number is wrong: " + varQueries['versionnum'][0])
#        quit()
        return 'VERSION NUMBER TOO HIGH'

# Appends an HTML element containing a link to a global text variable
# arguments: string, string
# returns: none
# side effects: uses a global variable, varContentText, to hold the 
#               constructed set of HTML links. Also, reads from 
#	            template files to pull the necessary HTML to 
#		        construct the final HTML file. In addition, if the
#               version number is off, the application will quit
# external calls: uses open(), close(), and quit()
def appendLine(textToAdd, namestr) :
    global varContentText

    varReplace0 = ""
    varReplace1 = ""
    varReplace2 = ""

    varTemplateFile = open("/Library/WebServer/CGI-Executables/indextemplates/linkcontainer.template", "rt")
    varTemplateText = varTemplateFile.read()
    varTemplateFile.close()

    if(retrieveQuery('cgipath') != 'VERSION NUMBER TOO HIGH'):
        varReplace0 = retrieveQuery('cgipath') + "?" + textToAdd
        varReplace1 = varTemplateText.replace("@@REPLACE1@@", varReplace0)
        varReplace2 = varReplace1.replace("@@REPLACE2@@", namestr) 
    else: 
        print("Content-Type: text/plain; charset=UTF-8")
        print()
        print("The version number of the supplied query is wrong")
        quit()

    varContentText = varContentText + varReplace2

# Percent-encodes a given string
# arguments: string
# returns: none
# external calls: calls urllib.parse.quote() to make the 
#                 encoded string
def encodeText(textToEncode) :
    return quote(textToEncode)

# Checks the file name extension and returns TRUE if it matches a pre-defined
# set. Otherwise, it returns FALSE
# arguments: none
# returns: boolean
def checkEnding(candidate):
    if (candidate.endswith("mpg")): 
        return True
    elif (candidate.endswith("mpeg")): 
        return True
    elif (candidate.endswith("wmv")): 
        return True
    elif (candidate.endswith("mp4")): 
        return True
    elif (candidate.endswith("avi")): 
        return True
    elif (candidate.endswith("flv")): 
        return True
    elif (candidate.endswith("mov")): 
        return True
    elif (candidate.endswith("apng")): 
        return True
    elif (candidate.endswith("avif")): 
        return True
    elif (candidate.endswith("gif")): 
        return True
    elif (candidate.endswith("jpg")): 
        return True
    elif (candidate.endswith("jpeg")): 
        return True
    elif (candidate.endswith("png")): 
        return True
    elif (candidate.endswith("svg")): 
        return True
    elif (candidate.endswith("webp")): 
        return True
    elif (candidate.endswith("bmp")): 
        return True
    elif (candidate.endswith("tif")): 
        return True
    elif (candidate.endswith("tiff")): 
        return True
    elif (candidate.endswith("mp3")): 
        return True
    elif (candidate.endswith("m4a")): 
        return True
    elif (candidate.endswith("aac")): 
        return True
    elif (candidate.endswith("flac")): 
        return True
    elif (candidate.endswith("ogg")): 
        return True
    else:
        return False

# Scans a given directory and processes files with the appropriate extensions
# arguments: none
# returns: none
# side effects: calls appendLine() and encodeText() to deliver data
#               these functions, in turn, modify a global variable
# external calls: os.walk(), file.endswith()
def scanDirectories() :
    for root, dirs, files in os.walk(retrieveQuery('scanpath')) :
        for file in files :
            if(checkEnding(file)):
                appendLine(encodeText(os.path.join(root, file)), file)

# Writes the text from a global variable to a file object held
# by a global variable
# arguments: none
# returns: none
# side effects: writes to a file held by a global variable, and 
#               pulls from a global variable. Also pulls HTML
#       		data from template files to get the HTML data
#		        for constructing the final HTML file
# external calls: open(), close(), write()
def writeContent() :
    global varContentText

    print("Content-Type: text/plain; charset=UTF-8", file=sys.stdout)
    print()
    print(varContentText, file=sys.stdout)

# -------------

# Walk through the directories and record the desired files
scanDirectories()

# Write the data into an HTML file
writeContent()
